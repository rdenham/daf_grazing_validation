=========
Changelog
=========

Version 2.0.0
==============
Updates for 20/21 run:
- FPC dataset updated to DP_QLD_S2_WOODY_FPC_2018.tif (Sentinel2 FPC clipped to woody extent)
- seasonal fractional cover dataset updated to cvmsre_qld_m201909201911_acaa2.tif
- 5 year summary statistics are now relative to specified season. 
- remove extraction of spatial arrangement data from jrsrp website (website broken). 

Version 1.0.0
==============

- Working version established. 
