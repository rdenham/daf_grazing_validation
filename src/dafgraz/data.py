import pkg_resources


def example_poly():
    '''
    Example polygon data in a json format.
    '''
    refimagePath = pkg_resources.resource_filename("dafgraz",
                                                   "data/example_data.json")
    return refimagePath
