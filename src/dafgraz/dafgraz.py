# -*- coding: utf-8 -*-
"""
Class and helper functions for the dafgraz package.
"""

import os
from urllib.request import Request, urlopen

import numpy as np
import rasterio as rio
from bs4 import BeautifulSoup
from rasterio import mask
from scipy.ndimage import label
import requests
import geojson

# from dafgraz import __version__

__author__ = "Robert Denham"
__copyright__ = "Robert Denham"
__license__ = "mit"


def read_url(url):
    """
    Get urls like you'd get files in a directory

    :param url: The url string (eg "http://qld.auscover.org.au/public/data/landsat/seasonal_fractional_cover/groundcover_drcm/qld/")
    :return: a list of filenames
    """
    filenames = []
    url = url.replace(" ", "%20")
    req = Request(url)
    a = urlopen(req).read()
    soup = BeautifulSoup(a, 'html.parser')
    x = (soup.find_all('a'))
    for i in x:
        file_name = i.extract().get_text()
        url_new = url + file_name
        url_new = url_new.replace(" ", "%20")
        if file_name[-1] == '/' and file_name[0] != '.':
            read_url(url_new)
        filenames.append(url_new)
    return filenames


def isSpring(imagename):
    """
    Given a standard imagename, determine if it is a spring image or not
    """
    baseimg = os.path.basename(imagename)
    return baseimg.split('_')[2][5:7] == '09'


class DAFGraz():
    """
    The class for a DAF Grazing object.
    """

    def __init__(self, geom):
        """
        Each DAFGraz object is based on a geometry.

        :param geom: geometry in 3577.

        """
        self.totarea = 0.0
        self.calculated_area = geom.area * 1e-4
        self.geometry = [geom]

    def calcMinDod(self, lastSeason, n):
        """
        Calculate the minimum DRCM values over the past n seasons

        :param n: number of previous seasons
        :return: minimum of the DRCM of those seasons
        """
        dodtifs = [i for i in
                   read_url(
                       "http://qld.auscover.org.au/public/data/landsat/seasonal_fractional_cover/groundcover_drcm/qld/")
                   if
                   i.endswith('tif')]
        # these will all sort, since they have a sensible naming convention
        dodtifs.sort()
        #find the file corresponding to lastSeason - this should be the end of the 5 year/20 season period
        lastSeasonIdx = dodtifs.index([dod for dod in dodtifs if lastSeason in dod][0])
        lastndods = dodtifs[lastSeasonIdx-20:lastSeasonIdx]
        
        drcm_fh = rio.open(lastndods[0])
        thisproject_drcm, thisproject_drcmaffine = mask.mask(drcm_fh, self.geometry, crop=True)
        dod_list = []
        for dodname in lastndods:
            drcm_fh = rio.open(dodname)
            thisproject_drcm, thisproject_drcmaffine = mask.mask(drcm_fh, self.geometry, crop=True)

            drcm = thisproject_drcm[0]
            dod_list.append(drcm)

        drcm_stack = np.stack(dod_list, axis=0)

        mindrc = np.zeros_like(drcm_stack[0,:,:]).astype('int16') + 9999
        for i in range(mindrc.shape[0]):
            for j in range(mindrc.shape[1]):
                vec = drcm_stack[:,i,j]
                vecvalid = vec[vec != 0]
                if len(vecvalid) > 5:
                    mindrc[i,j] = np.percentile(vecvalid, 20).astype('int16') - 100
        self.mindrcm = np.ma.masked_array(mindrc, mindrc == 9999)
        self.mindrcm_affine = thisproject_drcmaffine

    def writemindrcm(self, fname):
        """
        Write the median image to file
        :param fname: destination filename
        :return:
        """
        with rio.open(fname, 'w',
                      driver='GTiff',
                      height=self.mindrcm.shape[0],
                      width=self.mindrcm.shape[1],
                      count=1,
                      dtype=self.mindrcm.dtype,
                      crs='EPSG:3577',
                      transform=self.mindrcm_affine
                      ) as dst:
            dst.write(self.mindrcm, 1)

    def calcMaxLTCover(self):
        """
        Calculate maximum long term cover
        """
        minbg_fh = rio.open(
            "http://qld.auscover.org.au/public/data/landsat/seasonal_fractional_cover/groundcover_timeseries_statistics/lztmre_qld_e9016_djla2.tif")
        thisproject_minbg, self.longterm_max_cover_affine = mask.mask(minbg_fh, self.geometry, crop=True)
        mymask = np.where(thisproject_minbg == 0, True, False)
        self.longterm_max_cover = np.ma.masked_array(200 - thisproject_minbg, mask=mymask)[0]
        self.mean_longterm_max_cover = self.longterm_max_cover.mean()

    def writeMaxLTCover(self, fname):
        """
        Write the maximum long term cover image to file.

        :param fname: destination filename
        :return:
        """
        with rio.open(fname, 'w',
                      driver='GTiff',
                      height=self.longterm_max_cover.shape[0],
                      width=self.longterm_max_cover.shape[1],
                      count=1,
                      dtype=self.longterm_max_cover.dtype,
                      crs='EPSG:3577',
                      transform=self.longterm_max_cover_affine
                      ) as dst:
            dst.write(self.longterm_max_cover, 1)


    def calcPatchStats(self):
        """
        Produce the binary patch image (50% cover)
        """

        ground_cover_image = "/vsicurl/http://qld.auscover.org.au/public/data/landsat/seasonal_fractional_cover/groundcover_timeseries_statistics/spring-djr/lztmre_qld_e9016_djra2_ibare.tif"
        patch_size_ha = 1

        gcibare_fh = rio.open(ground_cover_image)
        thisproject_gcibare, thisproject_gcibareaffine = mask.mask(gcibare_fh, self.geometry, crop=True)
        mymask = (thisproject_gcibare == 0)
        valid_pixels = np.logical_not(mymask).sum()
        coverimg = np.where(thisproject_gcibare[2] > 150, 1, 0)

        # Find the continuous patches above 1 ha
        patch_list = []
        no_pixels = patch_size_ha * 10000 / 900
        labeled_array, num_features = label(coverimg)
        for num in range(1, num_features + 1):
            if len(np.where(labeled_array == num)[0]) > no_pixels:
                patch_list.append(num)
            else:
                labeled_array[labeled_array == num] = 0

        # reclassified_array =  np.where(labeled_array > 0, 1, 0)
        patches = np.zeros_like(labeled_array, dtype='uint8')
        patches[labeled_array > 0] = 1
        patcharea = patches.sum()
        self.patches = patches
        self.numpatches = len(patch_list)
        self.patch_percentage = patcharea / valid_pixels * 100
        self.patches_affine = thisproject_gcibareaffine

    def writepatches(self, fname):
        """
        Write the median image to file.

        :param fname: destination filename
        :return:
        """
        with rio.open(fname, 'w',
                      driver='GTiff',
                      height=self.patches.shape[0],
                      width=self.patches.shape[1],
                      count=1,
                      dtype=self.patches.dtype,
                      crs='EPSG:3577',
                      transform=self.patches_affine
                      ) as dst:
            dst.write(self.patches, 1)

    def calcCoverCategories(self):
        # Calculate the cover categories - Forest, high ground cover, medium ground cover and low ground cover
        # 
        fc_fh = rio.open(
            'http://qld.auscover.org.au/public/data/sentinel2/seasonal_fractional_cover/fractional_cover/qld/cvmsre_qld_m202009202011_acaa2.tif')
        thisproject_fc, thisproject_affine = mask.mask(fc_fh, self.geometry, crop=True)
        fpc_fh = rio.open('http://qld.auscover.org.au/public/data/visualisation/daf_grazing/DP_QLD_S2_WOODY_FPC_2019.tif')
        thisproject_fpc, thisproject_fpcaffine = mask.mask(fpc_fh,
                                                           self.geometry,
                                                           crop=True)
        # I think this is like:
        missing = (thisproject_fpc == 255) | (thisproject_fc[0] == 255)
        is_forest = (thisproject_fpc >= 11) & (thisproject_fpc != 254)
        is_high_gc = (thisproject_fc[0] <= 30)
        is_med_gc = (thisproject_fc[0] > 30) & (thisproject_fc[0] <= 70)
        is_low_gc = (thisproject_fc[0] > 70)
        condlist = [missing, is_forest, is_high_gc, is_med_gc, is_low_gc]
        choicelist = [0, 1, 2, 3, 4]
        fc_fpc_classified = np.select(condlist, choicelist)
        fc_fpc_classifiedm = np.ma.masked_array(fc_fpc_classified, mask=missing)
        fc_fpc_classifiedm.shape = fc_fpc_classifiedm.shape[1:]
        self.fc_fpc_classifiedm = fc_fpc_classifiedm.astype('int8')
        self.fc_fpc_classified_affine = thisproject_fpcaffine

    def writeclassified(self, fname):
        """
        Write the classified image to file.

        :param fname: destination filename
        :return:
        """
        with rio.open(fname, 'w',
                      driver='GTiff',
                      height=self.fc_fpc_classifiedm.shape[0],
                      width=self.fc_fpc_classifiedm.shape[1],
                      count=1,
                      dtype=np.dtype('int8'),
                      crs='EPSG:3577',
                      transform=self.fc_fpc_classified_affine
                      ) as dst:
            dst.write(self.fc_fpc_classifiedm, 1)

    def createPercentilesImage(self, lastSeason, gc_threshold=70):
        """
        Generate percentiles image. Calculate average area above threshold.
        Uses the 5 spring seasons up to and including lastSeason
        """
        
        # Get a sorted list of ground cover images from TERN server
        dixtifs = [i for i in
                   read_url("http://qld.auscover.org.au/public/data/landsat/seasonal_fractional_cover/ground_cover/qld/") if
                   i.endswith('tif')]
        # these will all sort, since they have a sensible naming convention
        dixtifs.sort(reverse=True)
        # Get list of last 5 spring images from the list of ground cover images
        allspringimages = [dix for dix in dixtifs if isSpring(dix)]
        lastSeasonImg = [s for s in allspringimages if lastSeason in s]
        lastSeasonIdx = allspringimages.index(lastSeasonImg[0])
        springimages = allspringimages[lastSeasonIdx:lastSeasonIdx+5]

        nullval, gcmin, gcmax = 0, 100, 200

        # Rank the pixels in the spring images
        # this will only work if you have enough valid
        # pixels in the spring image
        percentiles_list = []
        fifthpercentile = []
        percent_greater_than = []
        for dixname in springimages:
            try:
                gc_fh = rio.open(dixname)
                thisproject_gc, thisproject_gccaffine = mask.mask(gc_fh,
                                                                  self.geometry,
                                                                  crop=True)
                bareg = thisproject_gc[0]
                validpixels = (bareg >= gcmin) & (bareg <= gcmax)
                # convert to ground cover
                gc = (gcmax - bareg[validpixels]).clip(0, 100)
                percent_greater_than.append(sum(gc > gc_threshold) / np.sum(validpixels) * 100)
                percentiles = np.zeros_like(bareg)
                pct = np.percentile(gc, list(np.arange(100)))
                fifthpercentile.append(pct[5])
                pctatpoint = np.interp(gc, pct, np.arange(100))
                # this can be between 0 and 100,
                # percentiles[validpixels] = pctatpoint + 100
                percentiles[validpixels] = pctatpoint
                percentiles_list.append(percentiles)
            except Exception as e:
                print("WARNING: insufficient values to get percentiles")
        # Calculate the median rank for each pixel
        # make sure we had at least one valid set of values
        if len(percentiles_list) > 0:
            dstack = np.stack(percentiles_list, axis=0)
            mymask = np.where(dstack == 0, True, False)
            dsmstack = np.ma.masked_array(dstack, mask=mymask)
            medimage = np.ma.median(dsmstack, axis=0)
            self.mean5percentile = np.array(fifthpercentile).mean()
        else:
            medimage = np.zeros_like(bareg) + 255
            self.mean5percentile = np.nan
        self.medimage = medimage
        self.medimage_affine = thisproject_gccaffine
        # And get the average of the area > gc_threshold
        self.percent_greater_than = np.array(percent_greater_than).mean()
        

    def writemedimage(self, fname):
        """
        Write the median image to file.

        :param fname: destination filename
        :return:
        """
        with rio.open(fname, 'w',
                      driver='GTiff',
                      height=self.medimage.shape[0],
                      width=self.medimage.shape[1],
                      count=1,
                      dtype=self.medimage.dtype,
                      crs='EPSG:3577',
                      transform=self.medimage_affine
                      ) as dst:
            dst.write(self.medimage, 1)


    def summarise(self, lastSeason, numprevious=20, gc_threshold=70):
        """
        Calculate all images and produce a summary.
        :param lastSeason: The final season in the 5 year sequences for calMinDOD() and createPercentilesImage(),
                            specified like 'm201909201911'
        :param numprevious: The number of previous seasons (used in the call to calcMinDod)
        :param gc_threshold: The threshold used in createPercentilesImage.

        """
        self.calcMinDod(lastSeason, numprevious)
        self.calcMaxLTCover()
        self.calcPatchStats()
        self.createPercentilesImage(lastSeason, gc_threshold)
        self.calcCoverCategories()
        #self.getgeojson() #the jrsrp site currently (Sept '22) has problems which causes this call to fail.
        # now a pd dataframe of each of the outputs
        # make a dictionary first
        self.results = {'calculated_area': self.calculated_area,
                        'percent_greater_than': self.percent_greater_than,
                        'numpatches': self.numpatches,
                        'patch_percentage': self.patch_percentage,
                        'longterm_max_cover': self.mean_longterm_max_cover,
                        'minimum_DRCM': self.mindrcm.mean(),
                        'mean5thpercentile': self.mean5percentile}
        vals, counts = np.unique(self.fc_fpc_classifiedm, return_counts=True)
        labels = ['missing', 'forest', 'high_cover', 'medium_cover', 'low_cover']
        # first set all areas to zero
        for classlabel in labels:
            self.results[classlabel] = 0.0
        for i, val in enumerate(vals):
            try:
                self.results[labels[val]] = counts[i] * 0.01
                self.totarea = self.totarea + counts[i] * 0.01
            except:
                pass
        # we also want to express the cover parts as a percentage of
        # non-forested area
        nonforestarea = self.totarea - self.results['forest']
        if nonforestarea > 0:
            self.results['high_cover_percent'] = self.results['high_cover']/nonforestarea
            self.results['medium_cover_percent'] = self.results['medium_cover']/nonforestarea
            self.results['low_cover_percent'] = self.results['low_cover']/nonforestarea
        else:
            self.results['high_cover_percent'] = 0
            self.results['medium_cover_percent'] = 0
            self.results['low_cover_percent'] = 0

    def getgeojson(self):
        """
        Get the relevant spatial arrangement polygons defined
        by bounding box
        """
        minx, miny, maxx, maxy = self.geometry[0].bounds

        bounding_box = '{},{},{},{},epsg:3577'.format(minx, miny, maxx, maxy)

        url = 'https://map.jrsrp.com/geoserver/jrsrp/ows'

        params = dict(
            service='WFS',
            version='1.0.0',
            request='GetFeature',
            maxFeatures='200',
            typeName='jrsrp:spatialSegmentation',
            outputFormat='json',
            bbox=bounding_box,
            srsName='EPSG:3577',
        )

        resp = requests.get(url=url, params=params)
        self.geojson = resp.json()



    def writegeojson(self, fname):
        """
        Write the spatial arrangement geojson to file.

        :param fname: destination filename
        :return:
        """

        with open(fname, 'w') as f:
            geojson.dump(self.geojson, f)
