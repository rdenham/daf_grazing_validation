Introduction
=============

Summary information used (see :class:`dafgraz.dafgraz.DAFGraz`). For
each area of interest, four spatial products were generated. A summary
for each area of interest was generated using this spatial information.

The spatial products are described below.

Median Percentile Image
------------------------

See :func:`dafgraz.dafgraz.DAFGraz.createPercentilesImage`.

The median image is a summary of the `seasonal ground cover imagery
<http://www.auscover.org.au/datasets/seasonal-ground-cover/>`_ . It
takes the previous 5 spring images (that is based on the months
September, October and November). For each of these images, each pixel
is given a rank between 1 and 100. A pixel with a rank of 1 indicates
it is in the lowest 1% of all ground cover values in the area of
interest. A pixel with a rank of 100 indicates it is in the top
percentile for that image.

The 5 percentile images are summarised by taking the median. That is,
the value for  :math:`y_{i,j}` is given by

:math:`y_{i,j} = \mbox{median}\left[x_{i,j,1}, x_{i,j,2}, x_{i,j,3}, x_{i,j,4}, x_{i,j,5}\right]`

where :math:`x_{i,j,k}` is the percentile for pixel (*i*, *j* ) at date
*k*.

The output consists of an image of pixels between 0 and 100. Those
locations with larger values are those with consistently high cover
relative to the area of interest. Those locations with low values are
indicative of areas with consistently low cover, relative to all
pixels in the area of interest.


Lower 20th Percentile Seasonal Dynamic Reference Cover
-------------------------------------------------------

See :func:`dafgraz.dafgraz.DAFGraz.calcMinDod`.

Calculates the 20th percentile of the Seasonal Dynamic Refererence
Cover (SDRC) XX ADD link to data here XX 
over the past 20 seasons. The SDRC is a difference
product, comparing the cover at a location to a reference pixel. As
the reference pixel in general has high cover, the SDRC will in
usually  be negative (although for storage purposes the values are
typically offset by 100). The output will be an image with values
between -100 and +100. Large negative values indicate those locations
which in poor times have considerably lower cover than the reference
pixel. Small negative or positive values indicate those areas which
have cover closer to or exceeding the reference pixel.

The 20th percentile (rather than the minimum or a lower percentile)
was chosen, so that unusual events (such as fire) are not taken as
representative of typical poor conditions.


Bare Ground Patches
--------------------

See :func:`dafgraz.dafgraz.DAFGraz.calcPatchStats`.

This uses the long term spring season stats for bare ground from the
seasonal cover under trees product (band 1). It has a baseline 1986 -
2013 and is a six band product; 5th percentile, mean, median, 95th
percentile, std dev and count. It takes the third band (median bare
ground) and classifies each pixel into predominantly bare (median bare
proportion greater than 50%), or not bare (median bare proportion less
than or equal to 50%). Contiguous patches of these bare pixels that
are greater in area than 1 ha are identified. The result is an image
of 0's and 1's, where 1 indicates that the pixel belongs to a patch of
bare ground that is greater than 1 ha in area.

Cover Categories Image
-----------------------

See :func:`dafgraz.dafgraz.DAFGraz.calcCoverCategories`

The area of interest is classified into four classes: forest, high
ground cover, medium ground cover and low ground cover. The
classification is based on the Sentinel 2 based seasonal fractional
cover from Spring 2018, and the sentinel based two-year minimum
seasonal FPC derived rom seasonal reflectance composites from 2016
to 2017.

The decision rules are as follow:

1. Minimum FPC proportion is greater than 11% then forest.
2. Groundcover is greater than 70% then high ground cover.
3. Groundcover is between 30% and 70% then median ground cover.
4. Groundcover is less than 30% then low cover.

Each pixel is given a value between 1 and 4, in the order provided above.



Spatial Arrangement Data
------------------------

See :func:`dafgraz.dafgraz.DAFGraz.getgeojson`

An extract of the spatial arrangement data covering the area of
interest is generated.  This is a segmented vector layer, with a
number of cover related fields, and a final classification.

This is a preliminary draft of a condition classifier, and was
included as a demonstration only.


Summary Statistics
==================

A table of summary statistics is generated, with a row for each area of interest.

The fields in the table are as follows.

#. `calculated_area`: the area in hectares as calculated after transforming the
   area of interest into Australian Albers (EPSG:3577).
#. `percent_greater_than`: For the most recent 5 spring ground cover
   products, the percentage of the area of interest that is above 70%
   ground cover is calculated. This is then summarised by taking the
   mean of the 5 percentages.
#. `numpatches`: The number of bare patches greater than 1ha in area
   in the area of interest.
#. `patch_percentage`: The percentage of the area of interest that is
   occupied by bare patches of size greater than 1 ha.
#. `longterm_max_cover`: Uses the long term 5th percentile for bare
   ground from seasonal cover under trees product with a baseline
   1988 - 2013. The longterm maximum cover is the mean of the 5th
   percentile of bare ground.
#. `minimum_DRCM`: The mean over the area of interest of the lower
   20th percentile of the Seasonal Dynamic Reference Cover image.
#. `mean5thpercentile`: At each of the 5 previous spring seasons, we
   calculate 5th percentile of the ground cover for the area of
   interest. This is summarised for the area of interest by taking the
   mean of these 5 values.
#. `missing`: The area (in ha) of the area of interest for which there is no
   information from the FPC image or the sentinel based 2018 spring
   ground cover product.
#. `forest`: The area of forest as defined by the cover categories image.
#. `high_cover`: The area of high cover as defined by the cover
   categories image.
#. `medium_cover`: The area of medium cover as defined by the cover
   categories image.
#. `low_cover`: The area of low cover as defined by the cover
   categories image.
#. `high_cover_percent`: The area of high cover divided by the sum of
   the areas of high, medium and low cover.
#. `medium_cover_percent`: The area of medium cover divided by the sum of
   the areas of high, medium and low cover.
#. `low_cover_percent`: The area of low cover divided by the sum of
   the areas of high, medium and low cover.

