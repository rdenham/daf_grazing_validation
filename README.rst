=======
dafgraz
=======


Add a short description here!


Description
===========

A longer description of your project goes here...



Getting Started
===============

Using conda, we could::


    conda create -n dafgraz python=3.7 jupyterlab
    conda activate dafgraz
    git clone https://gitlab.com/rdenham/daf_grazing_validation.git
    cd daf_grazing_validation
    pip install -r requirements.txt

    # Install dafgraz
    python setup.py install

    # or just do
    # pip install git+https://gitlab.com/rdenham/daf_grazing_validation.git

    # Now explore the notebook
